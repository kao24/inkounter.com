<?php
if (isset($_GET['v'])) {
	$src = 'http://www.youtube.com/v/'.$_GET['v'];
}
else $src = '';
$params = 'version=3&autohide=0&hd=1&iv_load_policy=3&autoplay=1';

include_once('functions/curlFile.php');
$bigfile = curlFile('http://www.youtube.com/watch?v='.$_GET['v']);
preg_match('/<meta property="og:title" content=".+">/', $bigfile, $matches);
$title = substr($matches[0], 35, -2);
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="title" content="<?php echo $title; ?>" />
<meta name="description" content="Full video: http://youtube.com/watch?v=<?php echo $_GET['v']; ?>" />
<link rel="image_src" href="<?php echo 'http://i'.rand(1,4).'.ytimg.com/vi/'.$_GET['v'].'/default.jpg'; ?>" />
<?php include('parts/header.php');?>
</head>

<link href="<?php echo $src.'?'.$params; ?>" rel="video_src" />
<meta content="35" name="video_height" />
<meta content="400" name="video_width" />
<meta content="application/x-shockwave-flash" name="video_type" />
<embed type="application/x-shockwave-flash" flashvars="<?php echo $params; ?>" src="<?php echo $src; ?>" width="400" height="35" quality="best" wmode="transparent"></embed>
<br><br>

<?php include('parts/footer.php');
?>