<?php include('parts/header.php');?>

<i>Note: changes to the Flash player will require you to clear your browser cache in order for the changes to take immediate effect.</i>
<dl>
<dt><b>Feb. 18, 2013</b></dt>
	<dd>- Fixed folder permissions that prevented the system from being able to write files as expected.
			This problem must have been left over from when this project was moved to a new host server.</dd>
<dt><b>Jan. 16, 2013</b></dt>
	<dd>- Made adjustments to .htaccess to allow for more convenient access to .php pages without appending the .php
			file extension to the URL.</dd>
	<dd>- MP3 clips may now also be accessed via inkounter.com/{ID} rather than inkounter.com/fb.php?s={ID}. This older
			format of URL's will still work, so past sharing of the URL will maintain functionality. (The new format
			is just easier to use.)</dd>
	<dd>- Renamed older MP3 clips to their serial ID's. All functionality is maintained, unless people have been linking
			directly to any of these 7 files. (Unlikely.)</dd>
<dt><b>Sep. 2, 2012</b></dt>
	<dd>- Rearranged some HTML in the "Browse" page to better accomodate Internet Explorer. The page, when loaded in IE, should
			no longer complain when trying to access the Flash function to change the currently loaded sound clip.</dd>
<dt><b>Aug. 27, 2012</b></dt>
	<dd>- Implemented a fix to prevent the tool from updating the database for the same clip more than once.</dd>
	<dd>- Added an intermediary page to prevent users from accidentally uploading multiple copies of the same file.</dd>
<dt><b>Aug. 24, 2012</b></dt>
	<dd>- Modified the Album Art system to retrieve more (less-specific) results for metadata combinations where a field
			has parenthesized text.</dd>
<dt><b>Aug. 23, 2012</b></dt>
	<dd>- Rolled back implementation of Discogs' API because it returned too many poor results for album covers.</dd>
	<dd>- Removed all implementation of Last.FM's API...</dd>
	<dd>- ...because <a href="http://www.apple.com/itunes/affiliates/resources/documentation/itunes-store-web-service-search-api.html">
			Apple's search API</a> is so much better. (Faster and more reliable results.) Used as per a friend's recommendation.</dd>
	<dd>- With a single, reliable search API, the amount of duplicate album covers being shown is vastly decreased.</dd>
	<dd>- Added the new Album Art system to the clipping process; cycling through the covers will populate the Album input
			for the metadata with the cover's corresponding album title, as a suggestion to the user.</dd>
<dt><b>Aug. 22, 2012</b></dt>
	<dd>- Implemented a more rigorous method of finding album art. Now uses <a href="http://www.discogs.com/developers/">
			Discogs'</a> <a href="http://www.discogs.com/developers/resources/database/search-endpoint.html">Search</a> API,
			as well as an additional method of the previously used Last.FM API. My new implementation will now try to find
			album art based on a clip's Artist and Album metadata, if it exists. If not, it will try to find album art based
			on the clip's Artist and Track Title metadata, if it exists. Both attempts will also try to find a match without
			parenthesized text in the Album or Title if an exact match could not be found by one of the API's. In the 
			interest of efficiency, this method does not find album art for all Artist/Album and Artist/Title combinations,
			as querying the API's and receiving a response is a slow process.</dd>
	<dd>- Updated "fb.php", the share page, to provide users with additional options for a "Preview Thumbnail" when 
			embedded in Facebook. The page, when viewed, will now also provide users with an interface to cycle through 
			the found album art, if more than one was found.</dd>
	<dd>- The new album art search is not yet implemented in the clipping process.</dd>
<dt><b>Aug. 21, 2012</b></dt>
	<dd>- Added the ability for users to manually edit the metadata for their .mp3 clip during the clipping process.</dd>
	<dd>- Patched browse.php to allow for special HTML characters.</dd>
	<dd>- Made index.php ignore repeated spaces in metadata.</dd>
	<dd>- Patched a bug where the updates to the Flash player (see Aug. 20, 2012), where a Callback was added, prevented 
			the player from loading a sound file and auto-playing correctly when embedded in Facebook. The Callback is now 
			conditionally added, based on a new Flash variable that the player takes in during initialization, to restore
			expected/proper interaction and behavior in Facebook. (This updated the Flash player itself, and so will
			require a cleared cache for changes to take immediate effect.)</dd>
<dt><b>Aug. 20, 2012</b></dt>
	<dd>- Updated the Javascript code used during the clipping process to correctly check for valid inputs. (See Dec. 7, 2011)</dd>
	<dd>- Updated the Flash player's allowed inputs during the clipping process; it now interprets a 0 or empty value in the End field as 
			the end of the .mp3 file. Non-numerical values for "Fade-In" and "Fade-Out" are also automatically changed to accepted inputs.
	<dd>- Updated the Flash player to allow for Javascript interactivity. 
			There is no longer a need to re-enter values from the Flash player to the form during the clipping process. 
			The Flash player is now also capable of reloading a new .mp3 file via a Javascript command (as demonstrated on the "Browse" page).</dd>
	<dd>- Added a new <a href="browse">"Browse" page</a> for users to see what has been previously uploaded and clipped.</dd>
	<dd>- Added a &lt;!DOCTYPE html&gt; tag to accomodate IE browsers.</dd>
	<dd>- Improved the "flow" of the navigation bar.</dd>
<dt><b>Jun. 4, 2012</b></dt>
	<dd>- Updated the navigation bar to reduce clutter (and play with javascript).</dd>
<dt><b>Dec. 8, 2011</b></dt>
	<dd>- Patched the bug discovered Dec. 7, 2011.</dd>
<dt><b>Dec. 7, 2011</b></dt>
	<dd>- Removed a bug in Javascript code that incorrectly checked for valid inputs during the clipping process.</dd>
<dt><b>Dec. 4, 2011</b></dt>
	<dd>- OutSource now allows you to designate the file's artist and title for display in the Flash player.</dd>
	<dd>- New "Find on YouTube" button in Flash player pauses playback and opens a new tab/window 
			with a YouTube Search of the artist and/or title.</dd>
	<dd>- Fixed a bug where the player would not stay paused while the song file was still being loaded.</dd>
<dt><b>Nov. 14, 2011</b></dt>
	<dd>- Updated Flash player to start playback automatically only after a certain amount of the track has been loaded.
			Theoretically provides a smoother listening experience.</dd>
<dt><b>Nov. 12, 2011</b></dt>
	<dd>- Updated Flash player to allow mm:ss-format inputs in "edit" mode.</dd>
<dt><b>Nov. 8, 2011</b></dt>
	<dd>- Made more robust the code that reads metadata. Albums and tracks not catalogued by Last.fm will still be occasionally faulty.</dd>
	<dd>- 404 Error page now functional.</dd>
	<dd>- Enabled caching of image files, mp3's, and the Flash player.</dd>
<dt><b>Nov. 7, 2011</b></dt>
	<dd>- URLEncoded metadata passed to Flash player as necessary.</dd>
	<dd>- Removed Flash player's function of reading data directly from the file in order to maintain consistency.</dd>
<dt><b>Nov. 5, 2011</b></dt>
	<dd>- Updates to the Flash player. Made more vertically compact, has a download *button*, now has Edit mode, 
			smoother interactivity (slidable seeker and volume control).</dd>
	<dd>- Added fade-in/fade-out and sample clip playback support to the clipping process. 
			Not quite perfect yet, but it's getting there.</dd>
	<dd>- PHP now passes variables to Flash player for more consistent metadata when embedded into Facebook.</dd>
	<dd>- Reversed the order of these change log entries. Makes more sense this way.</dd>
<dt><b>Oct. 28, 2011</b></dt>
	<dd>- Bug fixes on the Flash player for when an invalid URL is supplied.</dd>
<dt><b>Oct. 27, 2011</b></dt>
	<dd>- Finished working on all playback functions of the Flash player. Not yet integrated with track-splitting.</dd>
<dt><b>Oct. 26, 2011</b></dt>
	<dd>- Published a partially functional Flash player I'm working on. Allows the use of fade-in. Not yet implemented 
			in the track-splitting process.</dd>
	<dd>- Added an <a href="/os">"OutSource" page</a> for embedding into Facebook sound files hosted on the internet.</dd>
<dt><b>Oct. 7, 2011</b></dt>
	<dd>- Added this page.</dd>
	<dd>- The site now pulls album cover art from Last.fm when it can, and uses the art as the preview image on Facebook. 
		If unable to pull an image from Last.fm, it defaults to the site logo.</dd>
	<dd>- The site also compares the metadata it reads from uploaded files to metadata available through Last.fm. This should 
		reduce typos and fix some characters prepended by the system.</dd>
</dl>

<?php
include('parts/footer.php');
?>