<?php
include('parts/header.php');

$connection = mysql_connect('localhost', $sqlUser, $sqlPass)  or die(mysql_error());
mysql_select_db('inkdb')  or die(mysql_error());
$query = mysql_query('select * from uploads order by ID') or die(mysql_error());
?>

<style type="text/css">
.browseCell {
	padding-left:10px;
	padding-right:10px;
}
.browse:hover {
	color:black;
	background-color:#f69f00;
	cursor:pointer;
}
</style>

<script type="text/javascript">
function trClick(file, artist, title, fin, fout) {
	if (shiftPressed)
		window.location = '/' + file;
	else
		document.getElementById('player').renewUrl('http://inkounter.com/winged/' + file + '.mp3', artist, title, fin, fout);
}

function togglePlayer() {
	if (document.getElementById('embeddedPlayer').style.visibility != 'hidden') {
		document.getElementById('embeddedPlayer').style.visibility = 'hidden';
		document.getElementById('showPlayer').style.visibility = 'visible';
	}
	else {
		document.getElementById('embeddedPlayer').style.visibility = 'visible';
		document.getElementById('showPlayer').style.visibility = 'hidden';
	}
}

shiftPressed = false;
document.onkeydown = function keyPress(event) {if (event.keyCode == 16) shiftPressed=true;}
document.onkeyup = function keyRelease(event) {if (event.keyCode == 16) shiftPressed=false;}
document.onselectstart = function() {return false;}		// prevents messiness from using shift+click
</script>

<table id="embeddedPlayer" style="position:fixed;bottom:0px;left:0"><tr>
	<td>
		<object type="application/x-shockwave-flash" id="player" name="player" data="parts/player.swf" width="400" height="46" quality="best">
			<param name="wmode" value="transparent" />
			<param name="flashvars" value="renewable" />
		</object>
	</td>
	<td valign="middle" style="font-size:30pt;background-color:#000000">
		<a href="javascript:togglePlayer()" style="text-decoration:none" title="Hide Flash Player">&laquo;</a>
	</td></tr>
</table>
<div id="showPlayer" style="position:fixed;bottom:0px;left:0;visibility:hidden;font-size:30pt">
	<a href="javascript:togglePlayer()" style="text-decoration:none" title="Show Flash Player">&raquo;</a>
</div>


This is the library of previously uploaded .mp3 clips.<br>
Click on any of the entries below to preview it.<br>
Shift+Click on any of the entries below to go to the clip's page (where you can then get the link to share on Facebook).
<br><br>

<table>
	<tr bgcolor="#006699">
		<td class="browseCell"><b>ID</b></td>
		<td class="browseCell"><b>Artist</b></td>
		<td class="browseCell"><b>Title</b></td>
		<td class="browseCell"><b>Album</b></td>
	</tr>
	<?php
	$rowCount = 0;
	while ($row = mysql_fetch_array($query)) {
		// removed early filename exceptions
		/*
		// early filename exceptions
		if ($row['id'] <= 7) {
			if ($row['id'] == 1)
				$file = 'No Sleep (Sex Ray Vision Cover)';
			elseif ($row['id'] == 2)
				$file = 'Fix Me';
			elseif ($row['id'] == 3)
				$file = 'Cypress Hill - Insane in the brain';
			elseif ($row['id'] == 4)
				$file = 'Too Lost In You';
			elseif ($row['id'] == 5)
				$file = 'That\\\'s the Way I Like It';
			elseif ($row['id'] == 6)
				$file = 'Breakeven1';
			elseif ($row['id'] == 7)
				$file = 'Shark In The Water';
		}
		else
		*/
			$file = $row['id'];
		$rowCount++; ?>
		<tr onclick="trClick('<?php echo $file; ?>', '<?php echo addslashes(htmlspecialchars($row['artist'])); ?>', '<?php echo addslashes(htmlspecialchars($row['title'])); ?>', <?php echo $row['fin']; ?>, <?php echo $row['fout']; ?>)"
			class="browse" <?php echo ($rowCount % 2 == 0) ? 'bgcolor="#0c0c0c"' : ''; ?>
			valign="top">
			<td class="browseCell" align="right"><?php echo $row['id']; ?></td>
			<td class="browseCell"><?php echo htmlspecialchars($row['artist']); ?></td>
			<td class="browseCell"><?php echo htmlspecialchars($row['title']); ?></td>
			<td class="browseCell"><?php echo htmlspecialchars($row['album']); ?></td>
		</tr>
	<?php } ?>
</table>

<?php
include('parts/footer.php');
?>
