<?php include('parts/header.php'); ?>

I don't have very many projects to share with you (yet), but I should mention <a href="http://discovr.perryhuang.com">"Discovr"</a>, 
a music discovery site based off of the Last.fm API. I made some small contributions to that (discontinued) project, which 
was put together under 24 hours for a Facebook Hackathon event that I attended with some friends. This is what got me into PHP 
and what pushed me to start a project on my own (this site).<br><br>

10/21/11 update: Me vs. Hackathon, take two. Just pitched our project, which we somewhat hastily titled, <a href="http://sms.perryhuang.com/">
"PipeSMS."</a> I was actually able to make substantial contributions this time around, since I'm much more comfortable with PHP 
than the first time around. This project is designed using API's to provide any phone capable of sending and receiving SMS text 
messages with the ability to pull certain data from online databases, particularly Yahoo!, the host of this Hackathon, and 
Google, although Wolfram is also referenced. More reason to be content with my flip phone.<br><br>

Yes. I said "flip phone."

<?php include('parts/footer.php'); ?>