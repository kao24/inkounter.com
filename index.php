<?php
include_once('parts/header.php');

//Check status of submission
if ((!isset($_POST['submit'])) && !isset($_POST['uploadRedirect']) && (!isset($_POST['timeinput']))) {
	// no submission yet ?>
	<b><font color="F75000">Attach sound clips to your Facebook status, comments, and wall posts.</font></b><br>
	This tool will take you through three (3) easy steps to embed audible tone in your Facebook persona.<br><br>
	Start by selecting an mp3 to upload:<br />
	<table bgcolor="#CCCCCC"><tr><td>
	<form enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
	<input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $maxsize ?>" />
	<input name="upfile" type="file" />
	<input type="submit" name="submit" value="Upload File" />
	</td></form>
	</table>
	<?php
}
elseif (!isset($_POST['uploadRedirect']) && !isset($_POST['timeinput'])) {
	// check filesize
	if (filesize($_FILES['upfile']['tmp_name']) < $maxsize) {
		// check extension
		$extcheck = explode('.', $_FILES['upfile']['name']);
		$s = sizeof($extcheck) - 1;
		if (strtolower($extcheck[$s]) == 'mp3') {
			$target_path = 'fledgling/'.basename($_FILES['upfile']['name']); //server save path
			if (move_uploaded_file($_FILES['upfile']['tmp_name'], $target_path)) {	// move the file
				$mp3 = 'fledgling/'.basename($_FILES['upfile']['name']); ?>
				<form method="POST" action="<?php echo $_SERVER['PHP_SELF'];?>" id="pageForm">
					<input type="hidden" name="uploadRedirect" value="1">
					<input type="hidden" name="filename" value="<?php echo $mp3;?>">
					Processing...<br>
					This should only take a few seconds. If you are not automatically redirected, click 
					<a href="javascript:document.getElementById('pageForm').submit()">here</a>.
				</form>
				<script type="text/javascript">
					document.getElementById('pageForm').submit();
				</script>
				<?php
				die;
			}
			else {?>
				A server-side error has occurred in uploading your file.<br>
				Try reducing the file size to less than <?php echo $maxsize/1000000;?>MB. <?php
			}
		}
		else {?>
			An error has occurred in uploading your file.<br>
			Make sure your file is an mp3.<?php
		}
	}
	else {?>
		An error has occurred in uploading your file.<br>
		Make sure your file is smaller than <?php echo $maxsize/1000000; ?>MB. <?php
	}
}
elseif (!isset($_POST['timeinput'])) {
	$mp3 = $_POST['filename'];

	// fetch metadata
	require_once 'functions/tagReader.php';
	$meta = tagReader($mp3);

	function correct($apiResults) {
		global $title, $artist;
		if ($artist != '')
			$artist = $apiResults[0]['artist'];
		if ($title != '')
			$title = $apiResults[0]['title'];
	}

	//////////////////////////////////////////////
	// correct artist/title/album, if possible
	include_once('functions/curlFile.php');
	$artist = (empty($meta['Artist'])) ? '' : trim($meta['Artist']);
	$title = (empty($meta['Title'])) ? '' : trim($meta['Title']);
	$album = (empty($meta['Album'])) ? '' : trim($meta['Album']);

	require_once('functions/getArt.php');
	$apiResults = getArt($artist, $title, $album);
	
	if (!empty($apiResults[0]))
		correct($apiResults);
	else {
		if (isset($meta['Artist']) && strlen($meta['Artist']) > 14)
			$artist = trim(substr($meta['Artist'],1));
		if (isset($meta['Title']) && strlen($meta['Title']) > 14)
			$title = trim(substr($meta['Title'],1));
		if (isset($meta['Album']) && strlen($meta['Album']) > 14)
			$album = trim(substr($meta['Album'],1));
		
		$apiResults = getArt($artist, $title, $album);
		
		if (!empty($apiResults[0]))
			correct($apiResults);
		else {
			$artist = (empty($meta['Artist'])) ? '' : trim($meta['Artist']);
			$title = (empty($meta['Title'])) ? '' : trim($meta['Title']);
			$album = (empty($meta['Album'])) ? '' : trim($meta['Album']);
		}
	}
	// end correcting artist/title/album
	//////////////////////////////////////////////

	//print metadata 	?>
	<style type="text/css">
		.darkInputText {
			background-color:#0f0f0f;
			border-color:#444444;
			color:#ffffff
		}
	</style>
	<form name="times" method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>" onsubmit="return validatetime()">
	<table><tr><?php
		//////////////////////////////////////////////
		// album art display-box (with navigation)
		include('parts/albumArtTD.php');
		//////////////////////////////////////////////?>
		<td valign="top"><table>
			<tr>
				<td width='10' />
				<td width='55'>Title: </td>
				<td><input type="text" class="darkInputText" id="titleField" name="Title" size="50"></td>
			</tr>
			<tr>
				<td width='10' />
				<td width='55'>Artist: </td>
				<td><input type="text" class="darkInputText" id="artistField" name="Artist" size="50"></td>
			</tr>
			<tr>
				<td width='10' />
				<td width='55'>Album: </td>
				<td><input type="text" class="darkInputText" id="albumField" name="Album" size="50"></td>
			</tr>
			<tr>
				<td colspan="3" align="right"><input type="button" onclick="resetForm()" value="Reset Metadata Fields"></td>
			</tr>
		</table></td></tr>
	</table><br>

	<dl>
	<dt><u>Directions</u></dt>
	<dd>- Define Start and End values for the clip by</dd>
		<dd class="in1">(a) click-and-dragging the labels in the Flash player, OR</dd>
		<dd class="in1">(b) typing values into the textboxes.</dd>
	<dd style="text-indent:0">These values can be in seconds or in mm:ss format. Decimals are allowed.</dd>
	<dd>- You can also enter (optional) values for fade-in and fade-out.</dd>
	<dd>- Click "Finish" when you are content with your inputs.</dd>
	<br>
	<font color="#FF0000">The clip length must be <b><?php echo $cliplength; ?></b> seconds or less.</font><br><br>
	<embed type="application/x-shockwave-flash" flashvars="src=<?php echo $mp3.'&edit=1'; ?>" src="parts/player.swf" width="400" height="46" quality="best" wmode="transparent">
	<br>

	<script type="text/javascript">
	function validatetime() {
		if (document.times.start.value == '' || document.times.end.value == '') {
			alert("Please fill out Start and End times in the HTML form before continuing.");
			return false;
		}
		else if ((document.times.fin.value != '' && isNaN(parseFloat(document.times.fin.value)))
				|| (document.times.fout.value != '' && isNaN(parseFloat(document.times.fout.value)))) {
			alert("Please use numeric inputs for fades.");
			return false;
		}
		else if (isNaN(converttime(document.times.start.value)) || isNaN(converttime(document.times.end.value))) {
			alert("Please use valid inputs for Start and End times.");
			return false;
		}
		//buggy, 6.5 not a number?
		else if (converttime(document.times.start.value) >= converttime(document.times.end.value)) {
			alert("This tool doesn't support time travel. Please be sure the clip doesn't end before it starts.");
			return false;
		}
		else return true;
	}
	function converttime(timein) {
		if (isNaN(parseFloat(timein))) {
			var timearray = timein.split(':');
			return Number(timearray[0])*60+Number(timearray[1]);
		}
		else return parseFloat(timein);
	}
	function fromFlash(start,end,fin,fout) {
		document.getElementById('start').value = start.toString().substring(0,8);
		document.getElementById('end').value = end.toString().substring(0,8);
		document.getElementById('fin').value = fin;
		document.getElementById('fout').value = fout;
	}
	function resetForm() {
		<?php
		if ($title != '') { ?>
			document.getElementById('titleField').value='<?php echo preg_replace('/[ ]+/',' ',addslashes($title)); ?>'; <?php
		}
		if ($artist != '') { ?>
			document.getElementById('artistField').value='<?php echo preg_replace('/[ ]+/',' ',addslashes($artist)); ?>'; <?php
		}
		if ($album != '') { ?>
			document.getElementById('albumField').value='<?php echo preg_replace('/[ ]+/',' ',addslashes($album)); ?>'; <?php
		} ?>
	}
	resetForm();	// initializes the form values; used javascript to do this to avoid having to deal with special characters in HTML
	</script>

		<input type="hidden" name="mp3" value="<?php echo basename($mp3); ?>">
		<input type="hidden" name="start" size="5" id="start">
		<input type="hidden" name="end" size="5" id="end">
		<input type="hidden" name="fin" size="3" id="fin">
		<input type="hidden" name="fout" size="3" id="fout">
		<input type="submit" name="timeinput" value="Finish">
	</form><?php
}
elseif (isset($_POST['timeinput'])) {
	// start and end times are input in mm:ss format
	$start = 0;				//default values
	$end = $cliplength;

	$mp3 = $_POST['mp3'];
	$start = $_POST['start'];
	$end = $_POST['end'];
	if ($_POST['fin'] != '')
		$fin = $_POST['fin'];
	else $fin = 0;
	if ($_POST['fout'] != '')
		$fout = $_POST['fout'];
	else $fout = 0;

	// convert times to seconds for function
	$startexp = explode(':', $start);
	if (array_key_exists(1, $startexp) && $startexp[0] >= 0)
		$start = $startexp[0] * 60 + $startexp[1];
	else if (array_key_exists(1, $startexp))
		$start = $startexp[1];

	$endexp = explode(':', $end);
	if (array_key_exists(1, $endexp) && $endexp[0] >= 0)
		$end = $endexp[0] * 60 + $endexp[1];
	else if (array_key_exists(1, $endexp))
		$end = $endexp[1];

	// CUT MY LIFE INTO PIECES, THIS IS MY LAST RESORT
	require_once('functions/class.mp3.php');

	$cut = new mp3;
	if ($end - $start > $cliplength) {
		?>Keep the clip shorter than <?php echo $cliplength; ?> seconds. To avoid copyright complications and to stay succinct
			(you don't want listeners to wonder why they're listening to what you posted).<br>
			If you want to post a full song, just post a link to YouTube or Soundcloud.<br><br>
			This tool has automatically deleted the file that you've just uploaded to prevent server overload, so you'll 
			have to start the clipping process back from the beginning. Don't like that? Follow directions next time.<?php
	}
	else if ($end - $start < 0) {
		?>This tool does not support time travel. Please make your end time somewhere <i>after</i> your start time.<?php
	}
	else {
		// add meta to database `uploads`
		// initialize php variables for sql input
		$artist = '';
		$title = '';
		$album = '';
		$artistfield = '';
		$titlefield = '';
		$albumfield = '';

		// where possible, set php variables for sql input
		// also work around any apostrophes and repeated spaces in metadata
		if (array_key_exists('Album', $_POST) && trim($_POST['Album']) != '') {
			$album = '\''.preg_replace('/[ ]+/',' ',addslashes(trim($_POST['Album']))).'\',';
			$albumfield = 'album,';
		}
		if (array_key_exists('Title', $_POST) && trim($_POST['Title']) != '') {
			$title = '\''.preg_replace('/[ ]+/',' ',addslashes(trim($_POST['Title']))).'\',';
			$titlefield = 'title,';
		}
		if (array_key_exists('Artist', $_POST) && trim($_POST['Artist']) != '') {
			$artist = '\''.preg_replace('/[ ]+/',' ',addslashes(trim($_POST['Artist']))).'\',';
			$artistfield = 'artist,';
		}

		// serialize the variable input combination, which should be unique
		$varCombo = array(
			'mp3' => $mp3,
			'start' => $start,
			'end' => $end,
			'fin' => $fin,
			'fout' => $fout,
			'artist' => $artist,
			'title' => $title,
			'album' => $album
		);

		session_start();
		$uploaded = FALSE;
		for ($i=0; $i<@sizeof($_SESSION['uploads']); $i++) {
			if ($varCombo == $_SESSION['uploads'][$i]) {
				$uploaded = TRUE;
				$sessionMatchIndex = $i;
				break;
			}
		}

		if (!$uploaded) {
			// open connection and execute query
			$connection = mysql_connect('localhost', $sqlUser, $sqlPass);

			mysql_select_db('inkdb');
			$query = mysql_query('insert into uploads ('.$artistfield.$titlefield.$albumfield.'fin,fout)
				values ('.$artist.$title.$album.$fin.','.$fout.');') or die(mysql_error());

			// serialize winged filename according to sql record id
			$sid = mysql_insert_id();

			// cut mp3 and return URL and metadata
			$wingedpath = 'winged/'.$sid.'.mp3';
			if (!$cut->cut_mp3('fledgling/'.$mp3, $wingedpath, $start, $end, 'second', false)) {
				?>An error has occurred in clipping the sound file.<br>
					Try again, starting from the beginning (by <a href="/">choosing an .mp3 file</a>).<br>
					If this error happens again, the file you're trying to upload and clip may be corrupted or incompatible
					with this site's functions. <a href="/contact">E-mail me</a> if this is the case.<?php
				//delete SQL record
				$query = mysql_query('delete from uploads where id='.$sid.');');
				mysql_query('alter table uploads auto_increment='.$sid.');');
				$clipError = true;
			}
			else {	// the clipping process was successful
				// set session variables
				$_SESSION['uploads'][] = $varCombo;
				$_SESSION['ids'][] = $sid;
			}
			mysql_close($connection);
		}
		else {	// the combo was already submitted
			$sid = $_SESSION['ids'][$sessionMatchIndex];
			$wingedpath = 'winged/'.$sid.'.mp3';
		}
		
		// show the result of clipping, if there was no error
		if (empty($clipError)) {
			// repeated spaces will handle themselves in the HTML
			$title = (array_key_exists('Title', $_POST)) ? trim($_POST['Title']) : '';
			$artist = (array_key_exists('Artist', $_POST)) ? trim($_POST['Artist']) : '';
			$album = (array_key_exists('Album', $_POST)) ? trim($_POST['Album']) : '';
			// echo resultant mp3 filepath
			?>
			<embed type="application/x-shockwave-flash" flashvars="src=http://inkounter.com/<?php echo $wingedpath; ?>&fin=<?php echo $fin;?>&fout=<?php echo $fout;?>"
			src="parts/player.swf" width="400" height="46" quality="best" wmode="transparent">
			</embed><br>

			<table><tr><?php
					include('parts/albumArtTD.php');?>
				<td valign="top"><table><?php
					if (array_key_exists('Title', $_POST) && trim($_POST['Title']) != '') { ?>
						<tr><td width='10' /><td width='55'>Title: </td><td><i><?php echo htmlspecialchars(trim($_POST['Title'])); ?></i></td></tr><?php
					}
					if (array_key_exists('Artist', $_POST) && trim($_POST['Artist']) != '') { ?>
						<tr><td width='10' /><td width='55'>Artist: </td><td><i><?php echo htmlspecialchars(trim($_POST['Artist'])); ?></i></td></tr><?php
					}
					if (array_key_exists('Album', $_POST) && trim($_POST['Album']) != '') { ?>
						<tr><td width='10' /><td width='55'>Album: </td><td><i><?php echo htmlspecialchars(trim($_POST['Album'])); ?></i></td></tr><?php
					}?>
				</table></td></tr>
			</table><br>

			Permalink to this clipped sound file: <a href="http://inkounter.com/<?php echo $sid; ?>">
			http://inkounter.com/<?php echo $sid; ?></a><br>
			You can "attach" (AKA "paste") this link to a Facebook wall post, status, or comment, and 
			anyone with Adobe Flash installed (the thing you need to watch YouTube videos) will be able to play your clip 
			without having to open a new browser tab/window.<br><br>
			Or, you can just use this handy little button:<br>
			<a target="_blank" href="http://www.facebook.com/sharer.php?u=http://inkounter.com/<?php echo $sid; ?>"><img src="images/Share_button.png"></a><?php
		}
	}
	@unlink('fledgling/'.$mp3);
}

include('parts/footer.php');
?>
