<?php include_once('parts/header.php');

if (!isset($_POST['send'])) { //forms not yet filled out 
	$_POST['name'] = '';
	$_POST['email'] = '';
	$_POST['subject'] = 'Hi!';
	$_POST['message'] = ''; ?>
	<font color="F75000">This email form doesn't actually send me anything</font>, since server mail is not enabled, but I got it working otherwise! 
	It gives error messages when appropriate, and doesn't clear the forms, forcing you to re-input everything like some sites 
	make you do. I hate that. If you actually want to talk to me, you're free to E-mail me at 
	<a href="mailto:inkounter@gmail.com">inkounter@gmail.com</a>.<br><br>
	Sorry about this. Hopefully I'll get things set up soon.<br><br> <?php
	include('parts/contactform.php');
}
elseif (isset($_POST['send']) && ($_POST['name'] != '') &&
									($_POST['email'] != '') &&
									($_POST['subject'] != '')&&
									($_POST['message'] != '') ) {
	$emailexpression = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
	if (preg_match($emailexpression, $_POST['email'])) {
		// this segment assumes that server mail is enabled
		$headers = 'From: '.$_POST['name'].' <'.$_POST['email'].'>\r\nReply-To: '.$_POST['name'].' <'.$_POST['email'].'>';
		if (preg_match($emailexpression, $_POST['email']))
			mail('arnoldkao@gmail.com', $_POST['subject'], $_POST['message'], $headers) or die('ERROR! ERRO! ERR! ER! E! !');
//		Message has been successfully sent. Below is a copy of the e-mail that you have sent.<br><br>

/*		// this segment assumes that cgi mail is enabled
		You are about to send the message as shown below.<br>
		When done verifying your message, click "Send."<br>
		<form action="/cgi-bin/cgiecho/parts/cgimail.txt" method="POST">
		<input type="hidden" name="name" value="<?php echo $_POST['name']; ?>">
		<input type="hidden" name="email" value="<?php echo $_POST['email']; ?>">
		<input type="hidden" name="subject" value="<?php echo $_POST['subject']; ?>">
		<input type="hidden" name="message" value="<?php echo $_POST['message']; ?>">
		<input type="submit" value="Send">
*/

		// show copy of submitted form
		?>
		<br><br>
		<table width="500">
		<tr><td width="80"><b>Name: </b></td><td><?php echo $_POST['name']; ?></td></tr>
		<tr><td width="80"><b>E-mail: </b></td><td><?php echo $_POST['email']; ?></td></tr>
		<tr><td width="80"><b>Subject: </b></td><td><?php echo $_POST['subject']; ?></td></tr>
		<tr><td colspan="2"></td></tr>
		<tr><td width="80"><b>Message: </b></td><td><?php echo $_POST['message']; ?></td></tr>
		</table><?php
	}
	else { ?>
		<font color="F75000">Check your E-mail address for typos.</font><br><br>
		<?php include('parts/contactform.php');		
	}
}
else { ?>
	<font color="F75000">Please fill out the form in its entirety.</font><br><br>
	<?php include('parts/contactform.php');
}
include_once('parts/footer.php'); ?>