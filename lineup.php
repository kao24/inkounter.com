<?php
if (array_key_exists('submission', $_POST)) {
	$artist = $_POST['artist'];
	$title = $_POST['title'];
	$album = $_POST['album'];
	$submission = true;
}
else {
	$artist = '';
	$title = '';
	$album = '';
	$submission = false;
}
?>

<script type="text/javascript">
function toggleInputs() {
	var element = document.getElementById('inputs');
	if (element.style.visibility != 'hidden')
		element.style.visibility = 'hidden';
	else
		element.style.visibility = 'visible';
}
</script>

<div id='inputs' style="position:fixed;top:0;left:0;background-color:#006699;width:100%;color:#ffffff;z-index:5">
	<form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
		Artist: <input type="text" name="artist" size="50" value="<?php echo $artist; ?>"><br>
		Title: <input type="text" name="title" size="50" value="<?php echo $title; ?>"><br>
		Album: <input type="text" name="album" size="50" value="<?php echo $album; ?>"><br>
		<input type="submit" name="submission" value="submit">
	</form>
</div>
<div style="position:fixed;top:0;right:0;z-index:10">
	<input type="button" value="show/hide" onclick="toggleInputs()">
</div>
<br><br><br><br><br><br><br>

<?php
if ($submission) {
	include_once('functions/getArt.php');

	$apiResults = getArt($artist, $title, $album);
	$imgResults = $apiResults[1];

	echo 'Total Request Time: '.$curlTime['total'].'<br>
			<a href="'.$curlTime['longestRequest'].'">Longest Request: '.$curlTime['longest'].'</a><br>
			Total Requests: '.$curlTime['count'].'<br>';
	for ($i=0; $i<sizeof($imgResults); $i++)
		echo '<img src="'.$imgResults[$i].'" />';
}

if (!empty($debug)) {
	echo "<br><br><b>View Page Source</b> to see the dumped variable.<br><br>";
	echo "\n\n\n\n";
	var_dump($debug);
}

?>