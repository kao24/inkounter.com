<?php include('parts/header.php'); ?>

Below is a list of features that I plan/hope to add in the future. If you come up with any other cool ideas or ideas 
as to how to implement some of the features already listed, 
<a href="contact">let me know</a>!

<dl>
<dt><b>Splitting Process</b></dt>
	<dd>- "Self-cut" support. For cases where Audacity is much more preferred. Still limit filesize, of course.</dd>
	<dd>- YouTube video support so you can paste a link and trim the video.</dd>
<dt><b>Playback</b></dt>
	<dd>- Re-encode mp3's. To ensure compatibility.</dd>
<dt><b>Personal Recording</b></dt>
<dt><b>OutSource</b></dt>
	<dd>- URL minimization (like tinyURL). Implementable through use of a separate SQL table.</dd>
	<dd>- User-specified album art/thumbnail.</dd>
<dt><b>YouTube Splitting</b></dt>
<dt><b>Social Aspects</b></dt>
	<dd>- User accounts. For "favorites."</dd>
	<dd>- Optional upload descriptions. With lyrics 'n' stuff. For enhanced browsing power. And for "presets." 
			The idea is that other people could find the uploads you make and use the same "presets" for themselves.</dd>
<dt><b>Game Time</b></dt>
	<dd>- "Name that song."</dd>
</dl>

<?php include('parts/footer.php'); ?>
