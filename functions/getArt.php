<?php
$fetch_iterations = 25;		// amount of data to retrieve from iTunes
$fetch_max = 10;			// max number of thumbnails to parse out before stopping the loop and returning the results

function getArt($artist, $title, $album) {
	global $fetch_max;

	$return = null;

	if (!empty($artist) || (!empty($title) && !empty($album))) {
		$regex = '/(\(.+\))*/';
		$artistPartial = trim(preg_replace($regex, '', $artist));
		$titlePartial = trim(preg_replace($regex, '', $title));
		$albumPartial = trim(preg_replace($regex, '', $album));
		$artistDifference = ($artist != $artistPartial);
		$titleDifference = ($title != $titlePartial);
		$albumDifference = ($album != $albumPartial);

		$return = itunes($artist, $title, $album);

		// if more results could be obtained by excluding parenthesized text, add them
		if (($artistDifference || $titleDifference || $albumDifference)
				&& (empty($return) || sizeof($return[1]) < $fetch_max)) {
			$result = itunes($artistPartial, $titlePartial, $albumPartial);
			for ($i=0; $i<sizeof($result[1]); $i++) {
				// if the return is not yet at full capacity and this result is not already in the return,
				if (sizeof($return[1]) < $fetch_max && (empty($return[1]) || !in_array($result[1][$i], $return[1]))) {
					$return[1][] = $result[1][$i];
					$return[2][] = $result[2][$i];
				}
			}
		}
	}

	return $return;
}

function itunes($artist, $title, $album) {
	global $debug;
	global $fetch_iterations;
	global $fetch_max;
	global $prevFetches;

	$input = trim($artist.' '.$title.' '.$album);	// idgaf what you tell me. I DO WHAT I WANT.
	// $return[0]['artist'] = null;
	// $return[0]['title'] = null;
	// $return[1] = null;	// album covers' url's
	// $return[2] = null;	// album names, corresponding to the covers
	$return = null;
	$itunes = curlFile('http://itunes.apple.com/search?limit='.$fetch_iterations.
														'&media=music'.
														'&entity=song'.
														'&term='.urlencode($input));
	if ($itunes) {
		$debug = $itunes = json_decode($itunes);

		for ($i=0; is_null($return) || $i<$fetch_iterations && sizeof($return[1])<$fetch_max-sizeof($prevFetches); $i++) {
			if (!empty($itunes->results[$i]) && !empty($itunes->results[$i]->artworkUrl100)) {
				$thumbnail = $itunes->results[$i]->artworkUrl100;
				if (!empty($thumbnail)) {
					$thumbnail = preg_replace('/[.]100x100-/', '.150x150-', $thumbnail);
					if ((is_null($return) || !in_array($thumbnail, $return[1]))
							&& (is_null($prevFetches) || !in_array($thumbnail, $prevFetches))) {
						$return[1][] = $thumbnail;
						$return[2][] = $itunes->results[$i]->collectionName;
					}
				}
			}
			else
				break;
		}
		
		if (!empty($itunes->results[0])) {
			// these values should only ever be used if the input metadata for artist/title is not empty
			$return[0]['artist'] = $itunes->results[0]->artistName;
			$return[0]['title'] = $itunes->results[0]->trackName;
		}

		if ($album != '' && @sizeof($return[1])+sizeof($prevFetches)<$fetch_max) {
			$prevFetches = @$return[1];

			$itunes = itunes($artist, $title, '');
			if (!is_null($itunes)) {
				foreach ($itunes[1] as $recursiveResult)
					$return[1][] = $recursiveResult;
				foreach ($itunes[2] as $recursiveResult)
					$return[2][] = $recursiveResult;

				// if $return[0] is still unset, populate it with the new recursive results (if they exist)
				if (empty($return[0]) && !empty($itunes->results[0])) {
					$return[0]['artist'] = $itunes->results[0]->artistName;
					$return[0]['title'] = $itunes->results[0]->trackName;
				}
			}
		}

	}
	return $return;
}

include_once('functions//curlFile.php');
?>