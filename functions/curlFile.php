<?php
function curlFile($url) {
	// file_get_contents throws an HTTP 400 error when the track is not found on audioscrobbler (the file doesn't exist)
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_USERAGENT, "inkounter.com");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$return = curl_exec($ch);
	
	//////////////////////////////////////////////
	// debugging variables
	global $curlTime;
	
	@$curlTime['count']++;
	$curlTime['this'] = curl_getinfo($ch, CURLINFO_TOTAL_TIME);
	
	if (array_key_exists('total', $curlTime))
		$curlTime['total'] += $curlTime['this'];
	else
		$curlTime['total'] = $curlTime['this'];

	if (!array_key_exists('longest', $curlTime) || ($curlTime['longest'] < $curlTime['this'])) {
		$curlTime['longest'] = $curlTime['this'];
		$curlTime['longestRequest'] = $url;
	}
	// end debugging variables
	//////////////////////////////////////////////

	curl_close($ch);
	return $return;
}
?>