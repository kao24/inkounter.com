<?php require_once('parts/header.php'); ?>

Below is a list of bugs of which I am aware. Fixes will be made if/when I ever find them and the time to implement them.<br>
If you come face-to-face with a bug that is not yet listed here, please <a href="contact">notify me</a>.

<dl>
<dt><b>Metadata</b></dt>
	<dd>- Asian characters will not be read/output properly.</dd>
	<dd>- In some cases, the function that reads ID3 values from mp3 files will read an extra character before the ID3 field.
			This appears to happen only when ID3 fields (title, artist, album) exceed 14 characters in length.
			This bug is also affected by what method is used in writing the ID3 values to the mp3 files.</dd>
		<dd class="in1"><b>Example:</b> editing metadata through Windows' right-click -> properties -> details will avoid this bug.
			Editing an ID3 field in Winamp will cause that field to be read (partially) incorrectly by this site.</dd>
	<dd>- In Windows Vista/7, you can revert any ID3 formatting made by programs like Winamp by <a href="http://windows.microsoft.com/en-US/windows-vista/Add-tags-or-other-properties-to-a-file">removing file properties</a>.
<dt><b>Clipping/Playback</b></dt>
	<dd>- Refuses to work on some mp3's. This could be due to any number of problems, very few of which (none, as far as I'm aware) are under my control.</dd>
		<dd class="in1"><b>Examples:</b> "fake" mp3's, <a href="http://www.draftlight.net/dnex/mp3player/mp3format.php">flash compatibility</a></dd>
	<dd>- Mp3's with 48000 Hz sampling rates will be clipped improperly, presumably as if the file were 44100 Hz.</dd>
	<dd>- Erratic behavior in playback on certain mp3's, notably due to faulty headers in the mp3 files. Run a Google search for a program to fix the file.</dd>
<dt><b>Contact Page</b></dt>
	<dd>- I have no idea what I'm doing. But don't tell anyone.</dd>

<?php include('parts/footer.php'); ?>
