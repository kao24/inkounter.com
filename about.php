<?php
include('parts/header.php');
?>

Welcome to my site. What you see is a constant work in progress, so please be patient as features and cleanliness
gradually string along.<br><br>

What this project is is a tool where you can upload mp3 files (one at a time), clip them through the simple
user interface provided here, and be given a link to post on Facebook, where a media player will then be embedded,
so that everyone can listen to your sound clip without the hassle of opening a new tab or a new window.<br><br>

This tool is targetted toward audiophiles (like myself) who would rather emphasize with tones than CAPS LOCK. 
Or, you know, uncommon punctuation and semantics. For things like sarcasm. This tool also makes it easier for me to 
quote lyrics from obscure and bizarre songs without people wondering whether I'm really the type of friend they want to have. 
Interpersonal relations are just so much easier to keep when we're on the same wavelength.<br><br>

As for who I am, aside from this site's builder/owner, my name's Arnold. I'm an undergraduate student at the University 
of Illinois at Urbana-Champaign. Studying Electrical Engineering, with a focus in Signal Processing, and an intent to earn
a Minor in Computer Science. Class of 2014. And, er... I like my music.<br><br>

I'm open to feedback, suggestions, and even plain ol' conversation if you are.
Feel free to reach me through the <a href="contact">Contact</a> page. Except with irrelevant advertising.
Like how to make 6 figures. From home. After taking a seminar. /facepalm.<br><br>

<?php
include('parts/footer.php');
?>
