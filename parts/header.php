<?php

// site variables
$cliplength = 60;			//max clip length in seconds
$maxsize = 10 * 1000000; 	//max file size in bytes (split to show MB)
							//remember to check "post_max_size" and "upload_max_filesize" in php.ini
include_once('parts/sqlCredentials.php');

if ($_SERVER['PHP_SELF'] != '/fb.php' && $_SERVER['PHP_SELF'] != '/os.php' && $_SERVER['PHP_SELF'] != '/yt.php') { ?>
	<!DOCTYPE html>
	<html>
	<head>
	<meta charset="UTF-8"><?php
}
if ($_SERVER['PHP_SELF'] != '/fb.php') { ?>
<title>
Inkounter
</title><?php } ?>
<link rel="stylesheet" type="text/css" href="/parts/styles.css">
<script type="text/javascript">
function showhide(element) {
	if (document.getElementById(element).style.visibility=='visible')
		document.getElementById(element).style.visibility='hidden';
	else
		document.getElementById(element).style.visibility='visible';
}
function highlight(element) {
	if (element.parentNode.parentNode.parentNode.bgColor=='#222222' && element.bgColor!='#333333') //parentNodes go: cell->row->table_body->table
		element.bgColor='#333333';
	else if (element.parentNode.parentNode.parentNode.bgColor=='#111111' && element.bgColor!='#555555')
		element.bgColor='#555555';
	else
		element.bgColor='';

	if (element.id=='navTools' || element.id=='navDev') {
		if (element.id=='navTools')
			textElement = document.getElementById('toolsText');
		else
			textElement = document.getElementById('devText');

		if (textElement.style.color!='rgb(246, 234, 0)' && textElement.style.color!='#F6EA00') //element.style.color is saved inconsistently in javascript
			textElement.style.color='#F6EA00';
		else
			textElement.style.color='#F69F00';
	}
}
var shiftPressed = false;
function redirect(e, pageUrl) {
	if (e.button == 0 && !shiftPressed)
		window.location=pageUrl;
}

var fbPostShown = false;
function expandFB() {
	if (!fbPostShown) {
		for (var i=0; i<2; i++)
			document.getElementById('fbPost'+i).style.display='';
		setTimeout('fbPostShown = true;', 20);
	}
	else {
		for (var i=0; i<2; i++)
			document.getElementById('fbPost'+i).style.display='none';
		setTimeout('fbPostShown = false;', 20);
	}
	// Timeouts prevent bubbling from running this same function twice in quick succession
}
</script></head>
<body link="#F69F00" vlink="#F69F00" alink="#F75000" text="#CCCCCC" bgcolor="#111111" style="margin:0; padding:0">

<div style="width:800px; background-color:#000000; left:0; right:0; margin:0px auto">
<a href="/"><img border="0" src="/images/banner.gif"></a>

<!--Navigation expandable tables-->
<table id="tools" style="margin-left:-400px"
onmouseover="showhide('tools');highlight(document.getElementById('navTools'))"
onmouseout="showhide('tools');highlight(document.getElementById('navTools'))" 
width="160" border="1" cellpadding="5" bgcolor="#111111">
	<tr><td onmouseover="highlight(this)" onmouseout="highlight(this)" onclick="expandFB()">
		<b><a href="javascript:expandFB()">FACEBOOK POST</a></b></font></td></tr>
	<tr id="fbPost0" style="display:none"><td onmouseover="highlight(this)" onmouseout="highlight(this)" onclick="redirect(event, '/')"
			style="padding-left:20px">
		<b>&raquo; <a href="/">UPLOAD</a><b></td></tr>
	<tr id="fbPost1" style="display:none"><td onmouseover="highlight(this)" onmouseout="highlight(this)" onclick="redirect(event, '/browse')"
			style="padding-left:20px">
		<b>&raquo; <a href="/browse">BROWSE</a><b></td></tr>
	<tr><td onmouseover="highlight(this)" onmouseout="highlight(this)" onclick="redirect(event, '/os')">
		<b><a href="/os">OUTSOURCE</a></b></td></tr>
</table>

<table id="dev" style="margin-left:-240px"
onmouseover="showhide('dev');highlight(document.getElementById('navDev'))"
onmouseout="showhide('dev');highlight(document.getElementById('navDev'))" 
width="160" border="1" cellpadding="5" bgcolor="#111111">
	<tr><td onmouseover="highlight(this)" onmouseout="highlight(this)" onclick="redirect(event, '/change')">
		<b><a href="/change">CHANGE LOG</a></font></b></td></tr>
	<tr><td onmouseover="highlight(this)" onmouseout="highlight(this)" onclick="redirect(event, '/bugs')">
		<b><a href="/bugs">KNOWN BUGS</a></font></b></td></tr>
	<tr><td onmouseover="highlight(this)" onmouseout="highlight(this)" onclick="redirect(event, '/plans')">
		<b><a href="/plans">CONCEPT IDEAS</a></font></b></td></tr>
</table>

<!--Navigation bar-->
<table height="30" width="100%" bgcolor="#222222"><tr>
	<td id="navTools" class="navBarEnd" align="center" width="160"
	onmouseover="showhide('tools');highlight(this);this.style.cursor='default'" 
	onmouseout="showhide('tools');highlight(this);this.style.cursor='auto'">
		<font style="color:#f69f00" id="toolsText"><b><u>TOOLS</u></b></font></td>
	<td id="navDev" class="navBar" align="center" width="160"
	onmouseover="showhide('dev');highlight(this);this.style.cursor='default'" 
	onmouseout="showhide('dev');highlight(this);this.style.cursor='auto'">
		<font style="color:#f69f00" id="devText"><b><u>DEVELOPMENT</u></b></font></td>

	<td class="navBar" align="center" width="160" onmouseover="highlight(this)" onmouseout="highlight(this)" onclick="redirect(event, '/projects')">
		<b><a href="/projects">PROJECTS</a></b></td>
	<td class="navBar" align="center" width="160" onmouseover="highlight(this)" onmouseout="highlight(this)" onclick="redirect(event, '/about')">
		<b><a href="/about">ABOUT</a></b></td>
	<td class="navBarEnd" align="center" width="160" onmouseover="highlight(this)" onmouseout="highlight(this)" onclick="redirect(event, '/contact')">
		<b><a href="/contact">CONTACT</a></b></td>
</td></tr></table>
<table cellpadding="15" width="100%">
	<tr><td height="100" valign="top">