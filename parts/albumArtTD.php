<?php
if (!isset($apiResults)) {
	require_once('functions/getArt.php');
	$apiResults = getArt($artist, $title, $album);
}


if (!empty($apiResults[1])) {
	$multipleCovers = (sizeof($apiResults[1]) > 1);
	if ($multipleCovers) { ?>
		<script type="text/javascript">
			var imageIndex = 0;
			var imageResults = new Array();
			var albumResults = new Array();
			var imageResultsSize = <?php echo sizeof($apiResults[1]);?>;
			<?php for ($i=0; $i<sizeof($apiResults[1]); $i++) {?>
				imageResults[<?php echo $i;?>] = '<?php echo $apiResults[1][$i];?>';
				albumResults[<?php echo $i;?>] = '<?php echo addslashes($apiResults[2][$i]);?>';
			<?php } ?>
			function imgPrev() {
				if (imageIndex == 0)
					imageIndex = imageResultsSize-1;
				else
					imageIndex--;

				albumUpdate();
			}
			function imgNext() {
				if (imageIndex == imageResultsSize-1)
					imageIndex = 0;
				else
					imageIndex++;

				albumUpdate();
			}
			function albumUpdate() {
				document.getElementById('imgNum').innerHTML = 'Album Art<br>('+(imageIndex+1)+'/'+imageResultsSize+')';
				document.getElementById('albumArt').src = imageResults[imageIndex];
				if (document.getElementById('albumField'))
					document.getElementById('albumField').value = albumResults[imageIndex];
			}
		</script><?php
	} ?>
	<td>
		<img src="<?php echo $apiResults[1][0]; ?>" id="albumArt" width="126" height="126" />
		<?php if ($multipleCovers) {?>
			<style type="text/css">
				a.imgNav {text-decoration:none; font-size:20pt;}
			</style>
			<br>
			<table width="100%"><tr valign="center">
				<td align="left"><a href="javascript:imgPrev()" class="imgNav">&laquo;</a></td>
				<td align="center" id="imgNum" style="font-size:10pt">Album Art<br>(1/<?php echo sizeof($apiResults[1]);?>)</td>
				<td align="right"><a href="javascript:imgNext()" class="imgNav">&raquo;</a></td>
			</tr></table><?php
		} ?>
	</td><?php
}
// end album art display-box
//////////////////////////////////////////////?>