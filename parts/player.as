//////////////////////////////////////////////
// INITIALIZE
//////////////////////////////////////////////

//define constants
var finVol = 0.05;				//volume for fade-in to start at

//pull variables passed through URL
var src:String = LoaderInfo(this.root.loaderInfo).parameters.src;
var fin:Number = LoaderInfo(this.root.loaderInfo).parameters.fin;
var fout:Number = LoaderInfo(this.root.loaderInfo).parameters.fout;
var edit:Number = LoaderInfo(this.root.loaderInfo).parameters.edit;			//edit mode?
var artist:String = LoaderInfo(this.root.loaderInfo).parameters.artist;
var title:String = LoaderInfo(this.root.loaderInfo).parameters.title;
var renewable:Boolean = (LoaderInfo(this.root.loaderInfo).parameters.renewable != null);		//enable renewUrl callback?

//default flashvars
if (!src) {
	src = "../winged/77.mp3";
	artist = "SNSD";
	title = "The Boys";
}
if (!fin)
	fin = 0;
if (!fout)
	fout = 0;
if (edit) {
	startTextInput.addEventListener(FocusEvent.FOCUS_IN, startInputFocus);
	startTextInput.addEventListener(FocusEvent.FOCUS_OUT, startInputBlur);
	startTextInput.addEventListener(KeyboardEvent.KEY_UP, startInputBox);

	endTextInput.addEventListener(FocusEvent.FOCUS_IN, endInputFocus);
	endTextInput.addEventListener(FocusEvent.FOCUS_OUT, endInputBlur);
	endTextInput.addEventListener(KeyboardEvent.KEY_UP, endInputBox);

	finTextInput.addEventListener(FocusEvent.FOCUS_IN, finInputFocus);
	finTextInput.addEventListener(FocusEvent.FOCUS_OUT, finInputBlur);
	finTextInput.addEventListener(KeyboardEvent.KEY_UP, finInputBox);

	foutTextInput.addEventListener(FocusEvent.FOCUS_IN, foutInputFocus);
	foutTextInput.addEventListener(FocusEvent.FOCUS_OUT, foutInputBlur);
	foutTextInput.addEventListener(KeyboardEvent.KEY_UP, foutInputBox);

	btnDownload.visible = false;
	btnSearch.visible = false;
	metaText.visible = false;
	startTab.visible = true;
	endTab.visible = true;
}
else {
	finText.visible = false;
	foutText.visible = false;
	finTextInput.visible = false;
	foutTextInput.visible = false;

	startText.visible = false;
	endText.visible = false;
	startTextInput.visible = false;
	endTextInput.visible = false;
}

//declare variables
var resumeTime:Number = 0;
var startTime:Number = 0;									//equals 0, unless changed in "edit" mode
var endTime:Number;											//equals the length of the track, unless changed in "edit" mode
var sampleTimer:Timer;
var trans:SoundTransform = new SoundTransform(0.75);		//initialize volume transform to 0.75
if (fin) trans.volume = 0;
var sound:Sound = new Sound();								//declare sound variable
var url:URLRequest = new URLRequest(src);
var volUsr:Number = 0.75;									//user-defined maximum volume,  default 75%
var isPlaying:Number;										//used in btnToggle function; keeps track of play/pause status
var hasPlayed:Number = 0;									//used to keep track of whether the track has begun playing
var isDraggingSeek:Number = 0;								//keeps track of whether user is dragging seek bar
var isDraggingVol:Number = 0;								//keeps track of whether user is dragging volume
var isDraggingStart:Number = 0;								//keeps track of whether user is dragging Start tab
var isDraggingEnd:Number = 0;								//keeps track of whether user is dragging End tab
var channel:SoundChannel = new SoundChannel();
var playInterval;
var currPosition:Number;									//property reference for refreshTrack (current track position)
var lastvalidend:String = '';
var lastvalidstart:String = '';

//set metadata
setMeta();

function setMeta() {
	if (artist && title)
		metaText.text = artist + ' - ' + title;
	else if (artist)
		metaText.text = 'Artist: ' + artist;
	else if (title)
		metaText.text = 'Title: ' + title;
	else {
		metaText.text = '';
		btnSearch.visible = false;
	}
}

//initialize buttons and event listeners
btnPlay.addEventListener(MouseEvent.CLICK, lstPlay);
btnPause.addEventListener(MouseEvent.CLICK, lstPause);
sound.addEventListener(IOErrorEvent.IO_ERROR, loadError);
btnDownload.addEventListener(MouseEvent.CLICK, download);
btnSearch.addEventListener(MouseEvent.CLICK, search);
volumeSlide.addEventListener(MouseEvent.MOUSE_DOWN, dragVol);
volumeBack.addEventListener(MouseEvent.MOUSE_DOWN, dragVol);
seeker.addEventListener(MouseEvent.MOUSE_DOWN, dragSeek);
seekpast.addEventListener(MouseEvent.MOUSE_DOWN, dragSeek);
loader.addEventListener(MouseEvent.MOUSE_DOWN, dragSeek);
startTab.addEventListener(MouseEvent.MOUSE_DOWN, dragStart);
endTab.addEventListener(MouseEvent.MOUSE_DOWN, dragEnd);
stage.addEventListener(MouseEvent.MOUSE_UP, dragStop);

//load sound file
sound.load(url);

//////////////////////////////////////////////
// JAVASCRIPT INTERACTION
//////////////////////////////////////////////
import flash.external.*; 

function renewUrl(newUrl:String, newArtist:String, newTitle:String, newFin:Number, newFout:Number) {
	if (sound.bytesLoaded < sound.bytesTotal)
		sound.close();
	if (playInterval) {
		soundPause();
		btnToggle(0);
		resumeTime = startTime;
		channel.removeEventListener(Event.SOUND_COMPLETE,lstStop);
		hasPlayed = 0;
	}

	artist = newArtist;
	title = newTitle;
	setMeta();

	fin = newFin;
	fout = newFout;

	sound.removeEventListener(IOErrorEvent.IO_ERROR, loadError);
	sound = new Sound();
	sound.addEventListener(IOErrorEvent.IO_ERROR, loadError);
	sound.load(new URLRequest(newUrl));

	loadInterval = setInterval(refreshLoad, 500);
}

if (renewable)
	ExternalInterface.addCallback("renewUrl", renewUrl as Function);
	//enables Javascript to call renewUrl() in Flash

//function that can be called by the Flash player to pass variables startTime, endTime, fin, fout to Javascript function fromFlash()
function toJavascript() {
	ExternalInterface.call("fromFlash", startTime/1000, endTime/1000, fin, fout);	//divide by 1000 to convert from ms to sec
}


//////////////////////////////////////////////
// SHOW LIVE FEEDS
//////////////////////////////////////////////

//show load progress
var loadInterval = setInterval(refreshLoad, 500);
var loaded:Number;

function refreshLoad() {
	loaded = sound.bytesLoaded / sound.bytesTotal;
	loader.width = loaded * 240;
	endTime = sound.length;							//endTime defaults to the end of the sound file
	if (loaded == 1) {								//stop updating loader when finished loading
		clearInterval(loadInterval);
		endTime = sound.length;
	}
	if ((sound.length >= 1500 || loaded == 1) && !hasPlayed)
		soundPlay();
}

//show track progress
function refreshTrack(playing:Number=1) {
	if (playing == 1)
		currPosition = channel.position;
	else
		currPosition = resumeTime				//exception made for when playback is paused
	if (currPosition > endTime) {
		soundPause();
		btnToggle(0);
		resumeTime = 0;
		channel.removeEventListener(Event.SOUND_COMPLETE,lstStop);
	}
	seekpast.width = currPosition / sound.length * 240;				//move seeker
	seeker.x = currPosition / sound.length * 240 + 31.7;
	trackTime.text = formatTime(currPosition) + " / " + formatTime(sound.length);	//update time display
	//check for fade-in/no-fade/fade-out stages of playback
	if (currPosition - startTime < fin * 1000) {
		trans.volume = volUsr * (currPosition - startTime) / (fin * 1000);
		if (trans.volume < 0)
			trans.volume = 0;
	}
	else if (currPosition > endTime - fout * 1000)
		trans.volume = volUsr * (endTime - currPosition) / (fout * 1200);
	else
		trans.volume = volUsr;
	channel.soundTransform = trans;
}


//////////////////////////////////////////////
// FUNCTIONS
//////////////////////////////////////////////

//playback functions
function soundPlay() {
	if (resumeTime < startTime)
		resumeTime = startTime;
	channel = sound.play(resumeTime,0,trans);
	btnToggle(1);
	channel.addEventListener(Event.SOUND_COMPLETE, lstStop);
	playInterval = setInterval(refreshTrack, 50);
	hasPlayed = 1;
}
function soundPause() {
	clearInterval(playInterval);
	resumeTime = channel.position;
	channel.stop();
}
function soundSeek() {
	if (this.mouseX < 40) {
		resumeTime = 0;
	}
	else if (this.mouseX > 274.5)
		resumeTime = sound.length - 1;
	else {
		resumeTime = (this.mouseX - 34.6) / 240 * sound.length;
	}
	refreshTrack(0);
}

//listener functions
function lstPlay(e:MouseEvent) {
	soundPlay();
}
function lstPause(e:MouseEvent) {
	soundPause();
	btnToggle(0);
}
function lstStop(e:Event) {
	clearInterval(playInterval);
	resumeTime = 0;
	btnToggle(0);
	channel.removeEventListener(Event.SOUND_COMPLETE,lstStop);
	//move seeker to the end
	seekpast.width = 242;
	seeker.x = 274.6;
	trackTime.text = formatTime(sound.length) + " / " + formatTime(sound.length);
}
function dragVol(e:MouseEvent) {
	stage.addEventListener(MouseEvent.MOUSE_MOVE, volMove);
	isDraggingVol = 1;
	volUsr = (this.mouseX - 360.35) / 32.5;
	volumeSlide.width = volUsr * 32.5;
	volumeSlide.height = volUsr * 12;
}
function volMove(e:MouseEvent) {
	if (this.mouseX > 360.35 && this.mouseX < 392.85)
		volUsr = (this.mouseX - 360.35) / 32.5;
	else if (this.mouseX < 360.35)
		volUsr = 0;
	else if (this.mouseX > 392.85)
		volUsr = 1;
	volumeSlide.width = volUsr * 32.5;
	volumeSlide.height = volUsr * 12;
	e.updateAfterEvent();
}
function loadError(e:IOErrorEvent) {
	if (loadInterval)
		clearInterval(loadInterval);
	if (playInterval)
		clearInterval(playInterval);
	metaText.htmlText = "<font color=\"#ff0000\">Invalid MP3 source URL</font>";
	stop();
}
function download(e:MouseEvent) {
	navigateToURL(new URLRequest(src));
}
function search(e:MouseEvent) {
	navigateToURL(new URLRequest("http://www.youtube.com/results?search_query="+metaText.text));
	soundPause();
	btnToggle(0);
}
function dragSeek(e:MouseEvent) {
	isDraggingSeek = 1;
	soundPause();
	soundSeek();
	stage.addEventListener(MouseEvent.MOUSE_MOVE, seekMove);
}
function seekMove(e:MouseEvent) {
	soundSeek();
	e.updateAfterEvent();
}
function dragStart(e:MouseEvent) {
	stage.addEventListener(MouseEvent.MOUSE_MOVE, startMove);
	isDraggingStart = 1;
}
function dragEnd(e:MouseEvent) {
	stage.addEventListener(MouseEvent.MOUSE_MOVE, endMove);
	isDraggingEnd = 1;
}
function startMove(e:MouseEvent) {
	if (this.mouseX >= 46.3 && this.mouseX <= 286.8)
		startTab.x = this.mouseX;
	else if (this.mouseX < 46.3)
		startTab.x = 46.3;
	else if (this.mouseX > 286.8)
		startTab.x = 286.8;
	startTime = Math.floor((startTab.x - 46.3) * sound.length / 240 + 1);		//math.floor(___ + 1) to round up
	if (channel.position < startTime) {
		if (isPlaying) {
			soundPause();
			btnToggle(0);
		}
		resumeTime = startTime;
	}
	startText.textColor = 0x333333;
	//int(value * 100)/100 rounds to 2 decimals
	startTextInput.text = formatTime(startTime,1);
	e.updateAfterEvent();
}
function endMove(e:MouseEvent) {
	if (this.mouseX >= 24.45 && this.mouseX <= 264.85)
		endTab.x = this.mouseX;
	else if (this.mouseX < 24.45)
		endTab.x = 24.45;
	else if (this.mouseX > 264.85)
		endTab.x = 264.85;
	endTime = Math.floor((endTab.x - 24.45) * sound.length / 240 + 1);			//math.floor(___ + 1) to round up
	if (channel.position > endTime) {
		if (isPlaying) {
			soundPause();
			btnToggle(0);
		}
		resumeTime = startTime;
	}
	endText.textColor = 0x333333;
	endTextInput.text = formatTime(endTime,1);
	e.updateAfterEvent();
}
function dragStop(e:MouseEvent) {
	if (isDraggingVol) {
		stage.removeEventListener(MouseEvent.MOUSE_MOVE, volMove);
		isDraggingVol = 0;
	}
	else if (isDraggingSeek) {
		stage.removeEventListener(MouseEvent.MOUSE_MOVE, seekMove);
		if (isPlaying)
			soundPlay();
		isDraggingSeek = 0;
	}
	else if (isDraggingStart) {
		stage.removeEventListener(MouseEvent.MOUSE_MOVE, startMove);
		isDraggingStart = 0;
		lastvalidstart = formatTime(startTime,1);
	}
	else if (isDraggingEnd) {
		stage.removeEventListener(MouseEvent.MOUSE_MOVE, endMove);
		isDraggingEnd = 0;
		lastvalidend = formatTime(endTime,1);
	}
}
function finInputFocus(e:FocusEvent) {
	finText.textColor = 0x333333;
}
function foutInputFocus(e:FocusEvent) {
	foutText.textColor = 0x333333;
}
function startInputFocus(e:FocusEvent) {
	startText.textColor = 0x333333;
}
function endInputFocus(e:FocusEvent) {
	endText.textColor = 0x333333;
}
function finInputBlur(e:FocusEvent) {
	if (finTextInput.text == '')
		finText.textColor = 0xFFFFFF;
}
function foutInputBlur(e:FocusEvent) {
	if (foutTextInput.text == '')
		foutText.textColor = 0xFFFFFF;
}
function startInputBlur(e:FocusEvent) {
	if (startTextInput.text == '')
		startText.textColor = 0xFFFFFF;
}
function endInputBlur(e:FocusEvent) {
	if (endTextInput.text == '') {
		endText.textColor = 0xFFFFFF;
		endTime = sound.length;
		endTab.x = 240 + 24.45;
		lastvalidend = '';
		//endTextInput.text = lastvalidend;

		toJavascript();
	}
}
function finInputBox(e:KeyboardEvent) {
	fin = Number(finTextInput.text);
	if (isNaN(fin)) {
		fin = 0;
		finTextInput.text = '0';
	}
	toJavascript();
}
function foutInputBox(e:KeyboardEvent) {
	fout = Number(foutTextInput.text);
	if (isNaN(fout)) {
		fout = 0;
		foutTextInput.text = '0';
	}
	toJavascript();
}
function startInputBox(e:KeyboardEvent) {
	startTime = deformatTime(startTextInput.text);
	if (startTime >= 0 && startTime <= sound.length) {
		startTab.x = startTime / sound.length * 240 + 46.3;
		lastvalidstart = endTextInput.text.toString();
	}
	else {
		startTime = 0;
		startTextInput.text = lastvalidstart;
	}
	if (channel.position < startTime) {
		if (isPlaying) {
			soundPause();
			btnToggle(0);
		}
		resumeTime = startTime;
	}
	toJavascript();
}
function endInputBox(e:KeyboardEvent) {
	endTime = deformatTime(endTextInput.text);
	if (endTime > 0 && endTime <= sound.length) {
		endTab.x = endTime / sound.length * 240 + 24.45;
		lastvalidend = endTextInput.text.toString();
	}
	//									backspace			delete				tab				shift				control				alt
	else if (endTime == 0 && e.keyCode != 46 && e.keyCode != 8 && e.keyCode != 9 && e.keyCode != 16 && e.keyCode != 17 && e.keyCode != 18) {
		endTime = sound.length;
		endTab.x = 240 + 24.45;
		lastvalidend = (sound.length / 1000).toString();
		endTextInput.text = lastvalidend;
	}
	else if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 16 || e.keyCode == 17 || e.keyCode == 18) {
		//do nothing
	}
	else {
		endTime = deformatTime(lastvalidend);
		endTextInput.text = lastvalidend;
	}
	toJavascript();
}


//////////////////////////////////////////////
// MISCELLANEOUS FUNCTIONS
//////////////////////////////////////////////

function btnToggle(p:Number) {
	if (p==1) {
		btnPlay.visible = false;
		btnPause.visible = true;
		isPlaying = 1;
	}
	else {
		btnPlay.visible = true;
		btnPause.visible = false;
		isPlaying = 0;
	}
}
function deformatTime(time:String):Number {
	var result:Number;
	var splittext:Array = time.split(":");
	if (splittext[1])
		return(splittext[0]*60000 + splittext[1]*1000);
	else
		return(result = splittext[0] * 1000);
}


//////////////////////////////////////////////
// OUTSOURCED
//////////////////////////////////////////////
function formatTime(time:Number,floor:Number=0):String {
	var sec:String;
	var min:String = Math.floor(time/60000).toString();
	if (floor)
		sec = ((time/1000)%60 < 10)? "0" + (int((time/1000)%60*100)/100).toString() : (int((time/1000)%60*100)/100).toString();
	else
		sec = (Math.floor((time/1000)%60) < 10)? "0" + Math.floor((time/1000)%60).toString() : Math.floor((time/1000)%60).toString();
	if (min == '0' && floor)
		return(sec);
	else
		return(min+":"+sec);
}