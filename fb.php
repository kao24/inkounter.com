<?php
// check for invalid URL's
if (($_GET['s'] != 'No Sleep (Sex Ray Vision Cover)') &&	// early filename exceptions
	($_GET['s'] != 'Fix Me') &&
	($_GET['s'] != 'Cypress Hill - Insane in the brain') &&
	($_GET['s'] != 'Too Lost In You') &&
	($_GET['s'] != 'That\'s the Way I Like It') &&
	($_GET['s'] != 'Breakeven1') &&
	($_GET['s'] != 'Shark In The Water') &&
	!preg_match('/^[0-9]+$/', $_GET['s'])) {
	?>
	<!DOCTYPE html>
	<html>
	<head>
	<meta charset="UTF-8"><?php
	include('parts/header.php');
	?>Invalid URL format<?php
	include('parts/footer.php');
}
else {
	// renaming the early files to correspond to their database entry ID's
	//$mp3path = 'http://inkounter.com/winged/'.$_GET['s'].'.mp3';

	// account for early filenames
	if (preg_match('/^[0-9]+$/', $_GET['s'])) $id=$_GET['s'];
	elseif ($_GET['s'] == 'No Sleep (Sex Ray Vision Cover)') $id=1;
	elseif ($_GET['s'] == 'Fix Me') $id=2;
	elseif ($_GET['s'] == 'Cypress Hill - Insane in the brain') $id=3;
	elseif ($_GET['s'] == 'Too Lost In You') $id=4;
	elseif ($_GET['s'] == 'That\'s the Way I Like It') $id=5;
	elseif ($_GET['s'] == 'Breakeven1') $id=6;
	elseif ($_GET['s'] == 'Shark In The Water') $id=7;

	$mp3path = 'http://inkounter.com/winged/'.$id.'.mp3';

	//fetch metadata from SQL database
	include_once('parts/sqlCredentials.php');
	$connection = mysql_connect('localhost', $sqlUser, $sqlPass);
	mysql_select_db('inkdb');
	$query = mysql_query('select * from uploads where id='.$id.';');
	$data = mysql_fetch_array($query);
	mysql_close($connection);
	$title = $data['title'];
	$artist = $data['artist'];
	$album = $data['album'];
	if ($data['fin'] != 0)
		$fin = '&fin='.$data['fin'];
	else $fin = '';
	if ($data['fout'] != 0)
		$fout = '&fout='.$data['fout'];
	else $fout ='';

	//fetch album art
	include_once('functions/getArt.php');
	$apiResults = getArt($artist, $title, $album);

	// prepare the html <title> tag
	if ($artist != '')
		$header = $artist.' - '.$title;
	else
		$header = $title;
	?>
	<!DOCTYPE html>
	<html>
	<head>
	<meta charset="UTF-8">
	<title><?php echo $header; ?> :: Inkounter.com</title>
	<meta name="title" content="<?php echo $header; ?>" />
	<meta name="description" content="No one reads this anyway." />
	<?php for ($i=0; $i<sizeof($apiResults[1]); $i++) { ?>
		<link rel="image_src" href="<?php echo $apiResults[1][$i];?>" />
	<?php } ?>
	<link rel="image_src" href="images/logo.jpg" />
	<?php include('parts/header.php');?>
	</head>

	<body>
	<link href="http://www.inkounter.com/parts/player.swf?src=<?php echo $mp3path.$fin.$fout.'&artist='.urlencode($artist).'&title='.urlencode($title); ?>" rel="video_src" />
	<meta content="46" name="video_height" />
	<meta content="400" name="video_width" />
	<meta content="application/x-shockwave-flash" name="video_type" />
	<embed type="application/x-shockwave-flash" flashvars="src=<?php echo $mp3path.$fin.$fout.'&artist='.urlencode($artist).'&title='.urlencode($title); ?>" src="parts/player.swf" width="400" height="46" quality="best" wmode="transparent"></embed>
	<br><br>
	<table><tr><?php
		include('parts/albumArtTD.php');?>
		<td valign="top"><table><?php
			if ($title != '') { ?>
				<tr><td width='10' /><td width='55'>Title: </td><td><i><?php echo htmlspecialchars(trim($title)); ?></i></td></tr><?php
			}
			if ($artist != '') { ?>
				<tr><td width='10' /><td width='55'>Artist: </td><td><i><?php echo htmlspecialchars(trim($artist)); ?></i></td></tr><?php
			}
			if ($album != '') { ?>
				<tr><td width='10' /><td width='55'>Album: </td><td><i><?php echo htmlspecialchars(trim($album)); ?></i></td></tr><?php
			}?>
		</table></td></tr>
	</table>

	<?php include('parts/footer.php');
}
?>
