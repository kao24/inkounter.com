<?php
$src = '';
$params ='';
if (isset($_GET['src'])) {
	$src = $_GET['src'];
	if (substr($src, 0, 7) != 'http://')
		$src = 'http://'.$src;
	$params = 'src='.$src;
	if (isset($_GET['fin']))
		$params .= '&fin='.$_GET['fin'];
	if (isset($_GET['fout']))
		$params .= '&fout='.$_GET['fout'];
	if (isset($_GET['artist']))
		$params .= '&artist='.urlencode($_GET['artist']);
	if (isset($_GET['title']))
		$params .= '&title='.urlencode($_GET['title']);
}
$imgsrc = '/images/logo.jpg';	//default thumbnail

if (isset($_GET['artist']) && isset($_GET['title']))
	$title = $_GET['artist'].' - '.$_GET['title'];
else $title = 'Inkounter OutSource';

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="title" content="<?php echo $title; ?>" />
<meta name="description" content="Pulled from: <?php echo $src;?>" />
<link rel="image_src" href="<?php echo $imgsrc; ?>" />
<?php include('parts/header.php');?>
</head>

<link href="http://www.inkounter.com/parts/player.swf?<?php echo $params; ?>" rel="video_src" />
<meta content="46" name="video_height" />
<meta content="400" name="video_width" />
<meta content="application/x-shockwave-flash" name="video_type" />
<embed type="application/x-shockwave-flash" flashvars="<?php echo $params; ?>" src="parts/player.swf" width="400" height="46" quality="best" wmode="transparent"></embed>
<br><br>

To embed <?php if ($src != '') {?>another<?php } else {?>a<?php }?> sound file hosted online into Facebook, use the form 
below and copy the resultant output link.
<br><br>

<script type="text/javascript">
var focusFNum = new Array(0, 0, 0, 0);
var base = new String();
var dir = new Number;

function defineBase() {
	base = 'http://inkounter.com/os?src='+encodeURIComponent(document.gen.urlsrc.value.trim());
	updateF(4);
}
function focusF(dir) {
	if (focusFNum[dir] == 0 || dir == 99) {
		if (dir == 0)
			document.gen.output.value += '&fin=';
		else if (dir == 1)
			document.gen.output.value += '&fout=';
		else if (dir == 2)
			document.gen.output.value += '&artist=';
		else if (dir == 3)
			document.gen.output.value += '&title=';
		focusFNum[dir]++;
	}
}
function blurF(dir) {
	if ((dir===0 && document.gen.fin.value.trim() === '') || (dir===1 && document.gen.fout.value.trim() === '')
			|| (dir===2 && document.gen.artist.value.trim() == '') || (dir===3 && document.gen.title.value.trim() == '')
			|| (dir===99)) {
		document.gen.output.value = 'http://inkounter.com/os?src='+encodeURIComponent(document.gen.urlsrc.value.trim());
		if (document.gen.fin.value && document.gen.fin.value.trim() != '')
			document.gen.output.value += '&fin='+document.gen.fin.value.trim();
		if (document.gen.fout.value && document.gen.fout.value.trim() != '')
			document.gen.output.value += '&fout='+document.gen.fout.value.trim();
		if (document.gen.artist.value && document.gen.artist.value.trim() != '')
			document.gen.output.value += '&artist='+encodeURIComponent(document.gen.artist.value.trim());
		if (document.gen.title.value && document.gen.title.value.trim() != '')
			document.gen.output.value += '&title='+encodeURIComponent(document.gen.title.value.trim());
		focusFNum[dir] = 0;
	}
}
function updateF(dir) {
	document.gen.output.value = 'http://inkounter.com/os?src='+encodeURIComponent(document.gen.urlsrc.value.trim());
	base = document.gen.output.value;
	if (dir != 0 && document.gen.fin.value)
		base = base + '&fin=' + document.gen.fin.value.trim();
	if (dir != 1 && document.gen.fout.value)
		base = base + '&fout=' + document.gen.fout.value.trim();
	if (dir != 2 && document.gen.artist.value)
		base = base + '&artist=' + encodeURIComponent(document.gen.artist.value.trim());
	if (dir != 3 && document.gen.title.value)
		base = base + '&title=' + encodeURIComponent(document.gen.title.value.trim());
	if (dir==0)
		document.gen.output.value = base + '&fin=' + document.gen.fin.value.trim();
	else if (dir==1)
		document.gen.output.value = base + '&fout=' + document.gen.fout.value.trim();
	else if (dir==2)
		document.gen.output.value = base + '&artist=' + encodeURIComponent(document.gen.artist.value.trim());
	else if (dir==3)
		document.gen.output.value = base + '&title=' + encodeURIComponent(document.gen.title.value.trim());
	else if (dir==4)
		document.gen.output.value = base;
}
</script>

<table>
<form action="#" name="gen">
<tr><td width="150">Sound file URL: </td><td><input type="text" name="urlsrc" size="70"
	value="<?php echo (isset($_GET['src']) ? urldecode($_GET['src']) : ''); ?>"
	onkeyup="defineBase()"></td></tr>
<tr><td width="150">Fade-in (Optional): </td><td><input type="text" name="fin" size="3"
	value="<?php echo (isset($_GET['fin']) ? urldecode($_GET['fin']) : ''); ?>"
	onfocus="focusF(0)"
	onkeyup="updateF(0)"
	onblur="blurF(0)"> sec</td></tr>
<tr><td width="150">Fade-out (Optional): </td><td><input type="text" name="fout" size="3"
	value="<?php echo (isset($_GET['fout']) ? urldecode($_GET['fout']) : ''); ?>"
	onfocus="focusF(1)"
	onkeyup="updateF(1)"
	onblur="blurF(1)"> sec</td></tr>
<tr><td width="150">Artist (Optional): </td><td><input type="text" name="artist" size="50"
	value="<?php echo (isset($_GET['artist']) ? urldecode($_GET['artist']) : ''); ?>"
	onfocus="focusF(2)"
	onkeyup="updateF(2)"
	onblur="blurF(2)">
<tr><td width="150">Title (Optional): </td><td><input type="text" name="title" size="50"
	value="<?php echo (isset($_GET['title']) ? urldecode($_GET['title']) : ''); ?>"
	onfocus="focusF(3)"
	onkeyup="updateF(3)"
	onblur="blurF(3)">
<tr><td colspan="2">&nbsp;</td></tr>
<tr><td width="150" align="center">Output: </td><td><input type="text" name="output" size="70"
	style="background-color: #222222; border-color: #999999; color: #ffffff"
	onclick="javascript:blurF(99);document.gen.output.focus();document.gen.output.select();"><br>
</td></tr>
</form>
</table>
<script type="text/javascript">
	defineBase();
</script>
<?php include('parts/footer.php');
?>
